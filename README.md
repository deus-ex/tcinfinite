# Infinite
Helpers that allow for some infinite stuff? 
(So far) contains unlimited skilled tools, persistent healing, unlimited energy.

## Installation
```ini
ServerPackages=TCInfinite
ServerActors=TCInfinite.InfiniteMutator
```

## Running
```
mutate inf
mutate inf.help
mutate inf.energy
mutate inf.heal
mutate inf.erase
mutate inf.inventory
mutate inf.nanokey
```
