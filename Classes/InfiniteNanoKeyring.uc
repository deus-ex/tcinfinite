class InfiniteNanoKeyRing extends NanoKeyring;

// Trust me, we always have the key
simulated function bool HasKey(Name KeyToLookFor){return true;}
simulated function bool TestMPBeltSpot(int BeltSpot){ return ( (BeltSpot >= 1) && (BeltSpot <=9) ); }

defaultproperties
{
     NoKeys="No Nano Keys Available!"
     KeysAvailableLabel="Nano Keys Available:"
     UseSound=Sound'DeusExSounds.Generic.KeysRattling'
     bDisplayableInv=False
     ItemName="Nanokey Ring"
     ItemArticle="the"
     PlayerViewOffset=(X=16.000000,Y=15.000000,Z=-16.000000)
     PlayerViewMesh=LodMesh'DeusExItems.NanoKeyRingPOV'
     PickupViewMesh=LodMesh'DeusExItems.NanoKeyRing'
     ThirdPersonMesh=LodMesh'DeusExItems.NanoKeyRing'
     Icon=Texture'DeusExUI.Icons.BeltIconNanoKeyRing'
     largeIcon=Texture'DeusExUI.Icons.LargeIconNanoKeyRing'
     largeIconWidth=47
     largeIconHeight=44
     Description="A nanokey ring can read and store the two-dimensional molecular patterns from different nanokeys, and then recreate those patterns on demand."
     beltDescription="KEY RING"
     bHidden=True
     Mesh=LodMesh'DeusExItems.NanoKeyRing'
     CollisionRadius=5.510000
     CollisionHeight=4.690000
     Mass=10.000000
     Buoyancy=5.000000
}
