class InfiniteMutator extends Mutator config(Mutators);

var() config bool bReplacePlayerInventory;
var() config bool bEraseDefaultSpawns;
var() config bool bUnlimitedEnergy;
var() config bool bHealPlayers;
var() config bool bGiveInfiniteNanokeyring;
var() config bool bUnlimitedAir;
var() config int DamagePercent;
var() config bool bReduceDamage;
var() config bool bSaveRespawnLocation;
var() config int RespawnTimeout;
var() config bool bAutoRespawn;

function PostBeginPlay() {
    local SkilledTool st;

    Level.Game.BaseMutator.AddMutator(Self);
    Level.Game.RegisterDamageMutator (Self);
    
    if(bEraseDefaultSpawns){
        foreach AllActors(class'SkilledTool', st) st.destroy();
    }

    SetTimer(1, true);
}

function Timer(){
    local DeusExPlayer dxp;
    
    foreach AllActors(class'DeusExPlayer', dxp) {
        if(bUnlimitedAir) dxp.swimTimer = dxp.swimDuration;
        
        if(bUnlimitedEnergy) dxp.Energy = dxp.EnergyMax;
    
        if(bHealPlayers){
            DXP.RestoreAllHealth();
            DXP.StopPoison();
            DXP.ExtinguishFire();
            DXP.drugEffectTimer = 0;
        }
    }
}

function bool hasResp(DeusExPlayer DXP){
    local TCIR Resp;
    
    foreach AllActors(class'TCIR', Resp){
        if(Resp.Player == DXP) return True;
    }
    return False;
}

function TCIR GetResp(DeusExPlayer DXP){
    local TCIR Resp;
    
    foreach AllActors(class'TCIR', Resp){
        if(Resp.Player == DXP) return Resp;
    }
}

function MutatorTakeDamage( out int ActualDamage, Pawn Victim, Pawn InstigatedBy, out Vector HitLocation, 
                        out Vector Momentum, name DamageType){
    local TCIR re;
    
    if(DeusExPlayer(InstigatedBy) != None && DeusExPlayer(Victim) != None && bReduceDamage) {
        ActualDamage = ActualDamage * (DamagePercent / 100);
    }

    if(DeusExPlayer(Victim) != None && bSaveRespawnLocation) {
        if(!hasResp(DeusExPlayer(Victim))){
            re = Spawn(class'TCIR');
            re.Player = DeusExPlayer(Victim);
            re.SetLocation(DeusExPlayer(Victim).Location);
            re.SetTimer(RespawnTimeout, False);
            re.LastDamageType = DamageType;
            //DeusExPlayer(Victim).ClientMessage("Respawn location saved.");
        } else {
            re = GetResp(DeusExPlayer(Victim));
            re.LastLocation = re.Location;
            re.bHasLastLocation = True;
            re.SetLocation(DeusExPlayer(Victim).Location);
            re.SetTimer(RespawnTimeout, False);
            re.LastDamageType = DamageType;
            //DeusExPlayer(Victim).ClientMessage("Respawn location updated.");
        }
    }
    super.MutatorTakeDamage(ActualDamage, victim, instigatedby, hitLocation, momentum, damagetype);
}

function ModifyPlayer(Pawn P) {
    local SkilledTool st;
    local inventory inv;
    local TCIR r;
    
    if(bReplacePlayerInventory){
        foreach AllActors(class'SkilledTool', st) {
            if(st.owner == p) st.destroy();
        }
        
        Inv=Spawn(class'InfiniteLockpick');
        Inv.Frob(p,None);
        Inv.bInObjectBelt = True;
        inv.Destroy();       
        
        Inv=Spawn(class'InfiniteMultitool');
        Inv.Frob(p,None);
        Inv.bInObjectBelt = True;
        inv.Destroy();
   }
   
   if(bGiveInfiniteNanokeyring){
        Inv=Spawn(class'InfiniteNanokeyring');
        Inv.Frob(p,None);
        Inv.bInObjectBelt = True;
        inv.Destroy();
   }
   
   if(bAutoRespawn && GetResp(DeusExPlayer(p)) != None) {
        r = GetResp(DeusExPlayer(p));
        
        if(r.LastDamageType == 'Fell'){
            if(r.bHasLastLocation){
                DeusExPlayer(P).SetCollision(false, false, false);
                DeusExPlayer(P).bCollideWorld = true;
                DeusExPlayer(P).GotoState('PlayerWalking');
                DeusExPlayer(P).SetLocation(GetResp(DeusExPlayer(P)).LastLocation);
                DeusExPlayer(P).ClientMessage("|P2Death zone unsafe, returning to last known safe location.");
            }  else  {
                DeusExPlayer(P).ClientMessage("|P2No safe respawn location available.");
                return;
            }
        } else {
            DeusExPlayer(P).ClientMessage("|P3Returning to death location.");
            DeusExPlayer(P).SetCollision(false, false, false);
            DeusExPlayer(P).bCollideWorld = true;
            DeusExPlayer(P).GotoState('PlayerWalking');
            DeusExPlayer(P).SetLocation(GetResp(DeusExPlayer(P)).location);
        }
        
        DeusExPlayer(P).SetCollision(true, true , true);
        DeusExPlayer(P).SetPhysics(PHYS_Walking);
        DeusExPlayer(P).bCollideWorld = true;
        DeusExPlayer(P).GotoState('PlayerWalking');
        DeusExPlayer(P).ClientReStart();
        GetResp(DeusExPlayer(P)).Destroy();
   }
   Super.ModifyPlayer(P);
}

function Mutate (String S, PlayerPawn PP){
    local HunterInfo h;
    local int hunters, hiders, total;
    local string marg, mval, mkey;
    
    //Keep the mutator chain linked
    Super.Mutate (S, PP);
    
    
    if(S ~= "inf.help") {
        PP.ClientMessage("Commands: inf.set <key> <val>, inf.respawn");
    }
    
    if(S ~= "inf") {
        PP.ClientMessage("Spawning with infinite items: "$bReplacePlayerInventory);
        PP.ClientMessage("Erasing default map items: "$bEraseDefaultSpawns);
        PP.ClientMessage("Infinite energy: "$bUnlimitedEnergy);
        PP.ClientMessage("Infinite healing: "$bHealPlayers);
        PP.ClientMessage("Spawning with Infinite Nanokey: "$bGiveInfiniteNanokeyring);
        PP.ClientMessage("Respawn: "$bSaveRespawnLocation$" ("$RespawnTimeout$"s)");
    }

    if(S ~= "inf.respawn"){
        if(GetResp(DeusExPlayer(PP)) != None){
            DeusExPlayer(PP).SetCollision(false, false, false);
            DeusExPlayer(PP).bCollideWorld = true;
            DeusExPlayer(PP).GotoState('PlayerWalking');
            DeusExPlayer(PP).SetLocation(GetResp(DeusExPlayer(PP)).location);
            DeusExPlayer(PP).SetCollision(true, true , true);
            DeusExPlayer(PP).SetPhysics(PHYS_Walking);
            DeusExPlayer(PP).bCollideWorld = true;
            DeusExPlayer(PP).GotoState('PlayerWalking');
            DeusExPlayer(PP).ClientReStart();
            GetResp(DeusExPlayer(PP)).Destroy();
        } else {
            PP.ClientMessage("You have no respawn point.");
        }
    }
    
    if(Left(s, 8) == "inf.set " && PP.bAdmin){
        marg = Right(S, Len(S) - 9);
        mval = Splitter(marg, " ", 0);
        mkey = Splitter(marg, " ", 1);
        BroadcastMessage("|P7Infinite setting changed by "$PP.PlayerReplicationInfo.PlayerName$": "$mkey$" = "$mval);
        SetPropertyText(mkey, mval);
        SaveConfig();
    }
    
}

function string Splitter(string s, string at, int index){
    local string outl, outr;
    
    outr = Right(s, Len(s)-instr(s,at)-Len(at));
    outl = Left(s, InStr(s,at));
    
    if(index == 0) return outr;
    else return outl;
}


defaultproperties
{
    bReplacePlayerInventory=True
    bEraseDefaultSpawns=True
    RespawnTimeout=120
    bSaveRespawnLocation=True
}
